# https://beta.atcoder.jp/contests/abc077/submissions/2097675
# ABC077 C
A,B,C=3.times.map{gets.split.map(&:to_i).sort}
 