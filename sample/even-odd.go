// この方のコードではじめてこのやりかたを知った
// https://beta.atcoder.jp/contests/abc050/submissions/1049496

package sample

func isOdd(x int) bool {
	return x&1 == 1
}

func isEven(x int) bool {
	return x&1 == 0
}
