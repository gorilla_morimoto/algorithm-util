package sample

import (
	"bufio"
	"fmt"
	"os"
)

// 1行ずつ読み込む
func readOneLine() {
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		fmt.Println(sc.Text())
	}
}

// 1単語ずつ読み込む（空白区切り）
func readOneWord() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)
	for sc.Scan() {
		fmt.Println(sc.Text())
	}
}
