package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(
		// slice, mapに初期値を与えるのと同じ感じで
		// 行を分割したときには,が必要になる
		// https://stackoverflow.com/questions/34846848/how-to-break-a-long-line-of-code-in-golang/34848928
		strings.ToUpper("hoge"),
		strings.ToUpper("fuga"),
	)

	fmt.Println(
		// 最終行に閉じカッコを記述するなら、最後の,は必要ないが
		// 視認性の観点からは避けたほうがいいと思う
		strings.ToUpper("foo"),
		strings.ToUpper("bar"))
}
