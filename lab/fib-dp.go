// DPでフィボナッチ数列のn項を求める
// https://www.slideshare.net/MonetoK/ss-56993100

package main

import "fmt"

func main() {
	fmt.Println(fibMemo(900))
	fmt.Println(fibDP(900))
}

func fibDP(n int) int {
	dp := make([]int, n)

	dp[0] = 1
	dp[1] = 1

	for i := 0; i < n-2; i++ {
		dp[i+2] = dp[i+1] + dp[i]
	}

	// 0-indexedなのでdp[n-1]を返す
	return dp[n-1]
}

var (
	// DPだと0: 1だけど、こっちでは0: 0になっていることに注意
	m = map[int]int{0: 0, 1: 1}
)

func fibMemo(n int) int {
	x, ok := m[n]
	if ok {
		return x
	}

	m[n] = fibMemo(n-2) + fibMemo(n-1)
	return m[n]
}
