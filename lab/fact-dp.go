package lab

func fact(n int) int {
	if n == 0 {
		return 0
	}

	dp := make([]int, n)
	dp[0] = 1
	for i := 1; i < n; i++ {
		dp[i] = dp[i-1] * (i + 1)
	}

	return dp[n-1]
}
